# Cafiine Installer

Installs Cafiine to System Memory without needing to compile with external resources.

## Warning
This was Designed for Ubuntu Linux - It uses powerpc64 utilities to compile.
This can be modified to your needs via Makefile

### Prerequisites
* Wii U (Fireware 5.0.0+ is recommended, should work from 4.1.0+)
* Homebrew Launcher
* Cafiine Server on Local Network

### Instructions

1. Modify `/src/main.c` on lines 109 through 112 to your Cafiine Server IP Address.
2. Modify `/Makefile` to fit your system needs. Your Operating System may require a different command which is configurable on line 12 in `/Makefile`
3. Run `make` and copy cafiine.elf to your Wii U SD Card
4. At this point, make sure you have a Cafiine Server running. [linkToMine]
5. Run cafiine.elf and select the game you wish to run, and follow on screen instruction. 

## Credits

Original Geckiine Installer (OatmealDome) on GitHub for the Layout and Installing Mechanics
Original Cafiine Server and Client Creator (NWPlayer123) on GitHUb
