#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include "main.h"
#include "dynamic_libs/os_functions.h"
#include "dynamic_libs/gx2_functions.h"
#include "dynamic_libs/socket_functions.h"
#include "dynamic_libs/sys_functions.h"
#include "dynamic_libs/fs_functions.h"
#include "dynamic_libs/vpad_functions.h"
#include "utils/logger.h"
#include "common/common.h"
#include "patcher/function_hooks.h"
#include "kernel/kernel_functions.h"
#include "cafiine/cafiine.h"

#define PRINT_TEXT1(x, y, str) { OSScreenPutFontEx(1, x, y, str); }
#define PRINT_TEXT2(x, y, _fmt, ...) { __os_snprintf(msg, 80, _fmt, __VA_ARGS__); OSScreenPutFontEx(1, x, y, msg); }

/* IP union */
typedef union u_serv_ip
{
	uint8_t  digit[4];
	uint32_t full;
} u_serv_ip;

static unsigned int patched = 0;

/* Entry point */
int Menu_Main()
{
	//!*******************************************************************
	//!                   Initialize function pointers                   *
	//!*******************************************************************
	//! do OS (for acquire) and sockets first so we got logging
	InitOSFunctionPointers();
	InitSocketFunctionPointers();
	InitGX2FunctionPointers();
	InitSysFunctionPointers();
	InitFSFunctionPointers();
	InitVPadFunctionPointers();
	
	log_init("192.168.1.64");
	
	SetupKernelCallback();
	
	log_printf("Geckiine started (rpx = '%s')\n", cosAppXmlInfoStruct.rpx_name);
	
	//!*******************************************************************
	//!                    Initialize BSS sections                       *
	//!*******************************************************************
	asm volatile(
				 "lis 3, __bss_start@h\n"
				 "ori 3, 3,__bss_start@l\n"
				 "lis 5, __bss_end@h\n"
				 "ori 5, 5, __bss_end@l\n"
				 "subf 5, 3, 5\n"
				 "li 4, 0\n"
				 "bl memset\n"
				 );
	
	PatchMethodHooks();
	
	if (strcasecmp("men.rpx", cosAppXmlInfoStruct.rpx_name) == 0)
	{
		log_printf("Returning to Wii U Menu Patchless.\n");
		
		log_deinit();
		return EXIT_RELAUNCH_ON_LOAD;
	}
	
	if (strlen(cosAppXmlInfoStruct.rpx_name) == 0)
	{
		log_printf("Returning to Wii U Menu.\n");
		
		SYSLaunchMenu();
		
		log_deinit();
		return EXIT_RELAUNCH_ON_LOAD;
	}
	
	if (!patched)
	{
		// Prepare screen
		int screen_buf0_size = 0;
		char msg[80];
		
		// Init screen and screen buffers
		OSScreenInit();
		screen_buf0_size = OSScreenGetBufferSizeEx(0);
		OSScreenSetBufferEx(0, (void *)0xF4000000);
		OSScreenSetBufferEx(1, (void *)0xF4000000 + screen_buf0_size);
		
		OSScreenEnableEx(0, 1);
		OSScreenEnableEx(1, 1);
		
		// Render IP selector and check for buttons
		u_serv_ip ip;
		ip.full = ( (192<<24) | (168<<16) | (2<<8) | (15<<0) );
		VPADData vpad_data;
		int error;
		int delay = 0;
		int gui_mode = 0;
		int sel_ip = 3;

		// Default IP Address
		ip.digit[0] = 192;
		ip.digit[1] = 168;
		ip.digit[2] = 1;
		ip.digit[3] = 64;

		while(1)
		{
			// Refresh GamePad screen
			OSScreenFlipBuffersEx(1);
			OSScreenClearBufferEx(1, 0);
			
			// Read vpad
			VPADRead(0, &vpad_data, 1, &error);
			
			// Title
			PRINT_TEXT1(23, 1, "-- Cafiine Server --");
			
			if (gui_mode == 0) // IP selector
			{
				PRINT_TEXT1(3, 4, "Press A to Install Cafiine to Memory.");
			}
			
			if ((vpad_data.btn_hold & BUTTON_A) && gui_mode == 0)
			{
				// Set wait message
				OSScreenClearBufferEx(1, 0);
				PRINT_TEXT1(43, 17, "Installing Cafiine...");
				OSScreenFlipBuffersEx(1);
				break;
			}
		}
		
		patched = 1;
		new_addr = ip.full;
	}
	
	log_printf("Returning to System Menu.\n");
	log_deinit();
	
	return EXIT_RELAUNCH_ON_LOAD;
}

// clear both buffers
void clearScreen()
{
	int i;
	for (i = 0; i < 2; i++)
	{
		int screen_buf0_size = OSScreenGetBufferSizeEx(0);
		int screen_buf1_size = OSScreenGetBufferSizeEx(1);
		
		// Clear screens
		OSScreenClearBufferEx(0, 0);
		OSScreenClearBufferEx(1, 0);
		
		// Flush the cache
		DCFlushRange((void *)0xF4000000, screen_buf0_size);
		DCFlushRange((void *)0xF4000000 + screen_buf0_size, screen_buf1_size);
		
		// Flip buffers
		OSScreenFlipBuffersEx(0);
		OSScreenFlipBuffersEx(1);
	}
}


